// CopyCtr.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <string>

template <typename T>
class CustomHolder
{
public:
	CustomHolder()
	{
		m_Info = new std::string("info");
		m_TotalSize = new size_t(0);

		m_Data.resize(0);
	}

	CustomHolder(int data_row, const std::string& info)
	{
		m_Info = new std::string(info);
		m_TotalSize = new size_t(0);

		m_Data.resize(data_row);
	}

	CustomHolder(const CustomHolder& other)
	{
		std::cout << "Copy Ctr\n";

		m_Data = other.m_Data;

		m_Info = new std::string(*(other.m_Info));
		m_TotalSize = new size_t(*(other.m_TotalSize));
	}

	CustomHolder& operator=(const CustomHolder& other)
	{
		if (this == &other) {
			return *this;
		}

		std::cout << "Assign operator\n";

		m_Data = other.m_Data;

		m_Info = new std::string(*(other.m_Info));
		m_TotalSize = new size_t(*(other.m_TotalSize));

		return *this;
	}

	~CustomHolder()
	{
		delete m_Info;
	}

	void SetData(int pos, const std::vector<T>& new_data)
	{
		if (pos >= m_Data.size() || pos < 0) {
			std::cout << "Out of range\n";
			return;
		}

		m_Data[pos] = new_data;
		(*m_TotalSize) += new_data.size();
	}

	void SetInfo(const std::string& new_info)
	{
		(*m_Info) = new_info;
	}

	void PrintHolderInfo() const
	{
		std::cout << "CustomHolder Info: " << *m_Info << '\n';
		std::cout << "Total size: " << *m_TotalSize << '\n';

		int i = 0;
		for (const auto& row : m_Data) {
			std::cout << i << ":";
			for (const auto& v : row) {
				std::cout << ' ' << v;
			}
			std::cout << '\n';
			++i;
		}
	}

private:
	std::vector<std::vector<T>> m_Data;
	std::string* m_Info;
	size_t* m_TotalSize;
};

int main()
{
	CustomHolder<int> db {5, "version 1.0.1"};
	db.SetData(0, { 1,2,3,4,5 });
	db.SetData(3, { 10,4,4,1 });
	db.SetData(4, { 11,22,33,44,55 });
	db.PrintHolderInfo();

	std::cout << "-----------------------------\n";
	CustomHolder<int> db2(db);
	db2.SetInfo("version 1.0.2");
	db2.SetData(1, { 0,0,0,0,0 });
	db2.PrintHolderInfo();

	std::cout << "-----------------------------\n";
	CustomHolder<int> db3;
	db3 = db2;
	db3.SetInfo("version 1.0.3");
	db3.SetData(2, { -1,-1,-1,-1,-1 });
	db3.PrintHolderInfo();
}


